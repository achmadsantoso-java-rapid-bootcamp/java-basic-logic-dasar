public class Logic1Soal3 {
    public static void main(String[] args) {
        soal3A(9);
        System.out.println("===== ke 1");
        soal3B(9);
        System.out.println("===== ke 2");
        soal3C(9);
        System.out.println("===== ke 3");
        soal3D(9);
        System.out.println("===== ke 4");
        soal3E(9);
        System.out.println("===== ke 5");

    }

    public static void soal3A(int a){
        //Masukkan angka ke deretA
        int[] deretA = new int[a];
        for (int i = 0; i < a; i++) {
            if (i == 0) {
                deretA[i] = 0;
            }else{
                deretA[i] = deretA[i-1]+2;
            }
        }

        //menampilkan hasil deretA
        for (Integer itemA : deretA){
            System.out.print(itemA+" \t");
        }
    }

    public static void soal3B(int b){
        //Masukkan angka ke deretB
        int[] deretB = new int[b];
        for (int j = 0; j < b; j++) {
            if (j == 0) {
                deretB[j] = 0;
            }else{
                deretB[j] = deretB[j-1]+2;
            }
        }

        //menampilkan hasil deretB
        for (Integer itemB : deretB){
            System.out.print(itemB+" \t");
        }
    }

    public static void soal3C(int c){
        //Masukkan angka ke deretC
        int[] deretC = new int[c];
        for (int k = 0; k < c; k++) {
            if (k == 0) {
                deretC[k] = 0;
            }else{
                deretC[k] = deretC[k-1]+2;
            }
        }

        //menampilkan hasil deretC
        for (Integer itemC : deretC){
            System.out.print(itemC+" \t");
        }
    }

    public static void soal3D(int d){
        //Masukkan angka ke deretD
        int[] deretD = new int[d];
        for (int l = 0; l < d; l++) {
            if (l == 0) {
                deretD[l] = 0;
            }else{
                deretD[l] = deretD[l-1]+2;
            }
        }

        //menampilkan hasil deretD
        for (Integer itemD : deretD){
            System.out.print(itemD+" \t");
        }
    }

    public static void soal3E(int e){
        //Masukkan angka ke deretE
        int[] deretE = new int[e];
        for (int m = 0; m < e; m++) {
            if (m == 0) {
                deretE[m] = 0;
            }else{
                deretE[m] = deretE[m-1]+2;
            }
        }

        //menampilkan hasil deretE
        for (Integer itemE : deretE){
            System.out.print(itemE+" \t");
        }
    }

}
