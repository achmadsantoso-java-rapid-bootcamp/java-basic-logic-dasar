public class Logic1Soal2 {
    public static void main(String[] args) {
        soal2A(9);
        System.out.println("===== ke 1");
        soal2B(9);
        System.out.println("===== ke 2");
        soal2C(9);
        System.out.println("===== ke 3");
        soal2D(9);
        System.out.println("===== ke 4");
        soal2E(9);
        System.out.println("===== ke 5");
    }

    public static void soal2A(int a){
        //masukkan angka ke deret
        int[] deret = new int[a];
        for (int i = 0; i < deret.length; i++) {
            if (i == 0){
                deret[i]=1;
            }else if (i % 2 == 0){
                deret[i] = deret[i-2] + 1;
            }else if (i==1){
                deret[i] = 3;
            }else {
                deret[i] = deret[i-2]+3;
            }
        }
        //tampilkan deret array
        for(Integer item : deret){
            System.out.print(item + "\t");
        }
    }

    public static void soal2B(int b){
        //masukkan angka ke deret array
        int[] deretb = new int[b];
        for (int j = 0; j< deretb.length;j++){
            if(j == 0){
                deretb[j] = 1;
            } else if (j==1) {
                deretb[j] = 3;
            } else if (j % 2 == 0) {
                deretb[j]= deretb[j-2]+1;
            }else {
                deretb[j]= deretb[j-2]+3;
            }
        }

        //tampilkan deret array
        for (Integer itemb: deretb){
            System.out.print(itemb+" \t");
        }
    }

    public static void soal2C(int c){
        //masukkan angka ke deret
        int[] deretC = new int[c];
        for (int k = 0; k < deretC.length; k++) {
            if (k == 0) {
                deretC[k] = 1;
            } else if (k == 1) {
                deretC[k] = 3;
            } else if (k % 2 == 0) {
                deretC[k] = deretC[k-2]+1;
            }else {
                deretC[k] = deretC[k-2]+3;
            }
        }

        //tampilkan asil deretC
        for(Integer itemC: deretC){
            System.out.print(itemC+" \t");
        }
    }

    public static void soal2D(int d){
        //masukkan angka ke deretD
        int[] deretD = new int[d];
        for (int l = 0; l < deretD.length; l++) {
            if (l == 0) {
                deretD[l]= 1;
            } else if (l == 1) {
                deretD[l] = 3;
            } else if (l %2 ==0) {
                deretD[l] = deretD[l-2]+1;
            }else {
                deretD[l] = deretD[l-2]+3;
            }
        }

        //tampilkan hasil deretD
        for(Integer itemD : deretD){
            System.out.print(itemD+" \t");
        }
    }

    public static void soal2E(int e){
        //masukkan angka ke deretE
        int[] deretE = new int[e];
        for (int m = 0; m < deretE.length; m++) {
            if (m == 0) {
                deretE[m] = 1;
            } else if (m == 1) {
                deretE[m] = 3;
            } else if (m % 2 ==0) {
                deretE[m] = deretE[m-2]+1;
            } else {
                deretE[m] = deretE[m-2]+3;
            }
        }

        //tampilkan hasil deretE
        for(Integer itemE: deretE){
            System.out.print(itemE+" \t");
        }
    }
}
