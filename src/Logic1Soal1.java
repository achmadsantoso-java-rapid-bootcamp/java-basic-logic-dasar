public class Logic1Soal1 {
    public static void main(String[] args) {
        soal1A(9);
        System.out.println("==== ke 1");
        soal1B(9);
        System.out.println("==== ke 2");
        soal1C(9);
        System.out.println("==== ke 3");
        soal1D(9);
        System.out.println("==== ke 4");
        soal1E(9);
        System.out.println("==== ke 5");
    }

    public static void soal1A(int n){
        for (int i= 1; i<=n; i++){
            System.out.print(i + " \t");
        }
    }

    public static void soal1B(int b){
        for (int z= 0; z<b; z++){
            System.out.print(z+1 + " \t");
        }
    }

    public static void soal1C(int c){
        for (int y = 1; y<=c; y++){
            System.out.print(y+" \t");
        }
    }

    public static void soal1D(int d){
        for(int x = 1; x<=d; x++){
            System.out.print(x+" \t");
        }
    }

    public static void soal1E(int e){
        for(int w= 1; w<=e; w++){
            System.out.print(w+" \t");
        }
    }



}
