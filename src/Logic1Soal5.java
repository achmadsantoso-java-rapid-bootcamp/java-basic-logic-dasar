public class Logic1Soal5 {
    public static void main(String[] args) {
        soal5A(9);
        System.out.println("===== ke 1");
        soal5B(9);
        System.out.println("===== ke 2");
        soal5C(9);
        System.out.println("===== ke 3");
        soal5D(9);
        System.out.println("===== ke 4");
        soal5E(9);
        System.out.println("===== ke 5");

    }

    public static void soal5A(int a){
        int[] deretA = new int[a];
        for (int i = 0; i < deretA.length; i++) {
            if(i==0 || i==1 || i==2){
                deretA[i] = 1;
                System.out.print(deretA[i] + " \t");
            }else{
                deretA[i] = deretA[i-1]+deretA[i-2]+deretA[i-3];
                System.out.print(deretA[i] + " \t");
            }
        }
    }

    public static void soal5B(int b){
        int[] deretB = new int[b];
        for (int j = 0; j < deretB.length; j++) {
            if(j==0 || j==1 || j==2){
                deretB[j] = 1;
                System.out.print(deretB[j] + " \t");
            }else{
                deretB[j] = deretB[j-1]+deretB[j-2]+deretB[j-3];
                System.out.print(deretB[j] + " \t");
            }
        }
    }

    public static void soal5C(int c){
        int[] deretC = new int[c];
        for (int k = 0; k < deretC.length; k++) {
            if(k==0 || k==1 || k==2){
                deretC[k] = 1;
                System.out.print(deretC[k] + " \t");
            }else{
                deretC[k] = deretC[k-1]+deretC[k-2]+deretC[k-3];
                System.out.print(deretC[k] + " \t");
            }
        }
    }

    public static void soal5D(int d){
        int[] deretD = new int[d];
        for (int l = 0; l < deretD.length; l++) {
            if(l==0 || l==1 || l==2){
                deretD[l] = 1;
                System.out.print(deretD[l] + " \t");
            }else{
                deretD[l] = deretD[l-1]+deretD[l-2]+deretD[l-3];
                System.out.print(deretD[l] + " \t");
            }
        }
    }

    public static void soal5E(int e){
        int[] deretE = new int[e];
        for (int m = 0; m < deretE.length; m++) {
            if(m==0||m==1||m==2){
                deretE[m] = 1;
                System.out.print(deretE[m]+ " \t");
            }else {
                deretE[m] = deretE[m-1]+deretE[m-2]+deretE[m-3];
                System.out.print(deretE[m]+ " \t");
            }
        }
    }
}
