public class Logic1Soal4 {
    public static void main(String[] args) {
        soal4A(9);
        System.out.println("===== ke 1");
        soal4B(9);
        System.out.println("===== ke 2");
        soal4C(9);
        System.out.println("===== ke 3");
        soal4D(9);
        System.out.println("===== ke 4");
        soal4E(9);
        System.out.println("===== ke 5");
    }

    public static void soal4A(int a){
        int[] deret = new int[a];
        //1  1  2  3  5  8  13  21
        for (int i=0; i<deret.length; i++){
            if (i==0 || i ==1){
                deret[i] = 1;
                System.out.print(deret[i] + " \t");
            }else {
                deret[i] = deret[i-2] + deret[i-1];
                System.out.print(deret[i] + " \t");
            }
        }
    }

    public static void soal4B(int b){
        int[] deretB = new int[b];
        for (int j = 0; j < deretB.length ; j++) {
            if (j==0 || j==1){
                deretB[j] = 1;
                System.out.print(deretB[j] + " \t");
            }else{
                deretB[j] = deretB[j-1] + deretB[j-2];
                System.out.print(deretB[j] + " \t");
            }
        }
    }

    public static void soal4C(int c){
        int[] deretC = new int[c];
        for (int k = 0; k < deretC.length; k++) {
            if (k == 0 || k == 1) {
                deretC[k] = 1;
                System.out.print(deretC[k] + " \t");
            }else {
                deretC[k] = deretC[k-1]+deretC[k-2];
                System.out.print(deretC[k] + " \t");
            }
        }
    }

    public static void soal4D(int d){
        int[] deretD = new int[d];
        //1  1  2  3  5  8  13  21
        for (int l=0; l<deretD.length; l++){
            if (l==0 || l==1){
                deretD[l] = 1;
                System.out.print(deretD[l] + " \t");
            }else {
                deretD[l] = deretD[l-2] + deretD[l-1];
                System.out.print(deretD[l] + " \t");
            }
        }
    }

    public static void soal4E(int e){
        int[] deretE = new int[e];
        //1  1  2  3  5  8  13  21
        for (int m=0; m<deretE.length; m++){
            if (m==0 || m==1){
                deretE[m] = 1;
                System.out.print(deretE[m] + " \t");
            }else {
                deretE[m] = deretE[m-2] + deretE[m-1];
                System.out.print(deretE[m] + " \t");
            }
        }
    }


}
